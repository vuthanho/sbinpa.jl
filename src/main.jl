using LinearAlgebra

export sbinpa

function sbinpa(X,r;epsilon=1e-4,maxiter=500,delta=1e-6)
    m,n=size(X)
    # Set of selected indices
    K = Vector{Int}(undef,m)
    # Norm of columns of input X
    normX0 = sum(abs2,X,dims=1)[:]
    # Max of the columns norm
    nXmax = maximum(normX0)
    # Init residual
    normR = copy(normX0)
    U = zeros(m,r+div(r*(r-1),2))
    Ht = zeros(n,r+div(r*(r-1),2))
    i = 1
    while i <= r
        a = maximum(normR)
        if sqrt(maximum(normR)/nXmax) < epsilon break end
        # Check ties up to 1e-6 precision
        b = findall((a .- normR)/a .<= 1e-6)
        # In case of a tie, select column with largest norm of the input matrix
        _, d = findmax(normX0[b])
        b = b[d]
        # Save index of selected column
        K[i] = b
        # Update residual
        if i == 1
            # At the first iteration, no bilinear terms
            @views copy!(U[:,1],X[:,b])
            Ht[:,1] .= nnlsHALS(X',U[:,[1]]',maxiter)
        else
            # Primary source
            @views copy!(U[:,i+div((i-1)*(i-2),2)],X[:,b]) 
            # Bilinear sources
            @views U[:,i+1+div((i-1)*(i-2),2):i+div(i*(i-1),2)] .= X[:,b].*X[:,K[1:i-1]] 
            Ht[b,i+div((i-1)*(i-2),2)] = 1.
            # Updating H
            @views fpgm!(X,U[:,1:i+div(i*(i-1),2)],Ht[:,1:i+div(i*(i-1),2)]',maxiter,delta)
        end
        normR .= sum.(abs2,eachcol(X-U*Ht'))
        i+=1
    end
    r = i-1
    # permuting to recover the order [1 ... r 1.*2 1.*3 ... (r-1).*r]
    lperm=Vector{Int}(undef,r+div(r*(r-1),2))
    for j in 1:r
        lperm[j] = j+div((j-1)*(j-2),2)
        for k in j+1:r
            lperm[r+r*(j-1)-div(j*(j-1),2)+k-j] = k+div((k-1)*(k-2),2)+j
        end
    end
    return K[1:r],U[:,lperm],copy(Ht[:,lperm]')
end

function nnlsHALS(X,H,iter)
    m,n=size(X)
    r=size(H,1)
    W = rand(m,r)
    HHt = H*H'
    XHt = X*H'
    for _ in 1:iter
        for j in axes(W,2)
            W[:,j] .+= (XHt[:,j].-W*HHt[:,j])./HHt[j,j]
            clamp!(view(W,:,j),0.0,Inf)
        end
    end
    return W
end

function fpgm!(X,W,H0,iter=500,delta=1e-6)
    nX = dot(X,X)
    WtX = W'*X
    WtW = W'*W
    L = opnorm(WtW)
    α1 = 1.0
    Hxtra = copy(H0)
    Hold = copy(H0)
    eps = 1.
    eps0 = 0.
    i = 1
    err = Vector{Float64}(undef,iter)
    while i <= iter && eps > delta*eps0
        # Saving previous iterate
        copy!(Hold,H0)
        # Accelerated projected gradient descent
        H0 .= Hxtra .+ 1/L .* (WtX.-WtW*Hxtra)
        clamp!(H0,0.,Inf)
        # Extrapolating
        α0 = α1
        α1 = 0.5*(1. +sqrt(1. +4. *α0^2))
        β = (α0-1)/α1
        Hxtra .= (1+β).*H0 .- β.*Hold
        # error
        err[i] = nX - 2*dot(H0,WtX) + dot(WtW,H0*H0')
        if i >= 2 && err[i] > err[i-1]
            copy!(Hxtra,H0)
        end
        if i == 1
            eps0 = norm(H0-Hold)
        end
        eps = norm(H0-Hold)
        i += 1
    end
    return nothing
end