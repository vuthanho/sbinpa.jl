# SbiNPA.jl

This is a Julia implementation of a **bilinear** version of the algorithm for *blind source separation of **linear-quadratic** near-separable mixtures* from this [paper](https://epubs.siam.org/doi/abs/10.1137/20M1382878) (arxiv version available [here](https://arxiv.org/abs/2011.11966)). Hence, this is an algorithm for blind source separation of **bilinear** near-separable mixtures.

## Recommended installation

* Start Julia
* Type `]` to enter `Pkg` mode
* Add my registry: `registry add https://gitlab.com/vuthanho/vuthanhoregistry`
* Install the SbiNPA package: `add SbiNPA`
* Leave `Pkg` mode with backspace ⌫
* `using SbiNPA`