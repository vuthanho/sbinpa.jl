using SbiNPA
using LinearAlgebra
using Random

m,n,r = 100,1000,10
rr = div(r*(r-1),2)
# ground truth U
U = rand(m,r)
# construction of W
W = Matrix{Float64}(undef,m,rr)
jbar = Vector{Vector{Int}}(undef,r)
jtild = Vector{Vector{Int}}(undef,r)
for j in 1:r
    jbar[j] = vcat(1:j-1,j+1:r)
    jtild[j] = vcat([(k-1)*r-div(k*(k+1),2)+j for k in 1:j-1],[(j-1)*r-div(j*(j-1),2)+k for k = 1:(r-j)])
end

function updateW!(U,W,jbar,jtild)
    for j in axes(U,2)
        @. W[:,jtild[j]] = U[:,j]*U[:,jbar[j]]
    end
end

updateW!(U,W,jbar,jtild)

# ground truth H
H = [I rand(r,n-r)]
H = vcat(H,hcat(zeros(rr,r),0.5*rand(rr,n-r)))
normalize!.(eachcol(H),1)
# Computing synthetic separable bilinear dataset
X = [U W]*H

# Solving separable bilinear NMF
@time K,Ws,Hs = sbinpa(X,r)